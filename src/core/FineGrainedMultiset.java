/**
 * FineGrainedMultiset.java
 * This class implements a Multiset, to be parameterized
 * with a generic type T.
 *
 * Synchronization is provided through fine-grained locking.
 *
 * @author: Cosimo Agati
 */

package core;
import java.util.Collection;

public class FineGrainedMultiset<T> implements Multiset<T> {
	private MultisetElement<T>[] buckets;
	private CollectionEraser<T> collectionEraser;

	@SuppressWarnings({"unchecked", "rawtypes"})
	public FineGrainedMultiset(int size) {
		buckets = new MultisetElement[size];
		collectionEraser = new CollectionEraser<T>(this);

		for (int i = 0; i < size; ++i) {
			buckets[i] = new MultisetElement<T>(null);
			MultisetElement<T> next = new MultisetElement<T>(null);
			buckets[i].setNext(next);
			next.setPrevious(buckets[i]);
		}
	}

	/*
	 * Constructor with default size.
	 */
	public FineGrainedMultiset() { this(1024); }

	/*
	 * Gets the bucket index to start looking for the element.
	 */
	private int getBucketIndex(Object element) {
		return element.hashCode() % buckets.length;
	}

	/*
	 * Returns a node with element hash value greater or equal
	 * to the desired element's hashCode.
	 *
	 * IMPORTANT: after returning, the node and its predecessor
	 * WILL BE LOCKED.
	 */
	private MultisetElement<T> lookup(Object element) {
		final int BUCKET_INDEX = getBucketIndex(element);
		final int ELEMENT_HASH = element.hashCode();
		MultisetElement<T> previous = buckets[BUCKET_INDEX];
		MultisetElement<T> node = previous.getNext();

		previous.lock();
		node.lock();
		while (node.getElementHash() < ELEMENT_HASH) {
			MultisetElement<T> next = node.getNext();
			previous.unlock();
			next.lock();
			previous = node;
			node = next;
		}
		return node;
	}

	/**
	 * Adds an element to the multiset.
	 * 
	 * @param element The element to be added.
	 * @return true if the operation is successful, false otherwise.
	 * @throws IllegalArgumentException if element == null.
	 */
	@Override
	public boolean add(T element) {
		return add(element, 1) > 0;
	}

	/**
	 * Checks if an element is currently in the multiset.
	 * 
	 * @param element The element to be checked.
	 * @return true if the operation is successful, false otherwise.
	 */
	@Override
	public boolean contains(Object element) {
		MultisetElement<T> node = lookup(element);
		MultisetElement<T> previous = node.getPrevious();
		boolean found = node.getElementHash() == element.hashCode();

		previous.unlock();
		node.unlock();
		return found;
	}

	/**
	 * Returns the number of occurrences of an element in the
	 * multiset.
	 * 
	 * @param element The element whose occurrences are being counted.
	 * @return The number of occurrences of element.
	 */
	@Override
	public int count(Object element) {
		MultisetElement<T> node = lookup(element);
		MultisetElement<T> previous = node.getPrevious();
		int occurrences = node.getOccurrences();

		previous.unlock();
		node.unlock();
		return occurrences;
	}

	/**
	 * Removes an element from the multiset.
	 * 
	 * @param element The element to be removed
	 * @return true if the operation is successful, false otherwise.
	 * @throws IllegalArgumentException if element == null.
	 */
	@Override
	public boolean remove(Object element) {
		return remove(element, 1) > 0;
	}

	/**
	 * Adds a number of occurrences of the same element to
	 * the multiset.
	 * 
	 * @param element The element to be added.
	 * @param occurrences The number of occurrences to add.
	 * @return The number of elements successfully inserted.
	 * @throws IllegalArgumentException if element == null.
	 */
	@Override
	public int add(T element, int occurrences) {
		MultisetElement<T> node = lookup(element);
		MultisetElement<T> previous = node.getPrevious();

		if (node.getElementHash() == element.hashCode()) {
			node.increaseOccurrences(occurrences);
		} else {
			MultisetElement<T> newNode =
				new MultisetElement<T>(element,
						occurrences, previous, node);
			previous.setNext(newNode);
			node.setPrevious(newNode);
		}
		previous.unlock();
		node.unlock();
		return occurrences;
	}

	/**
	 * Removes a number of occurrences of the same element from
	 * the multiset.
	 * 
	 * @param element The element to be removed
	 * @param occurrences The number of occurrences to remove
	 * @return The number of elements successfully removed.
	 * #throws IllegalArgumentException if element == null.
	 */
	@Override
	public int remove(Object element, int occurrences) {
		MultisetElement<T> node = lookup(element);
		MultisetElement<T> previous = node.getPrevious();

		if (node.getElementHash() == element.hashCode())
			occurrences = removeFromExisting(node, occurrences);
		else
			occurrences = 0;
		previous.unlock();
		node.unlock();
		return occurrences;
	}

	/*
	 * If the element to be removed is found, remove the desired number
	 * of occurrences. If the number of occurrences present in the
	 * multiset before removal is less or equal to the number of
	 * occurrences to be removed, the node is deallocated.
	 */
	private int removeFromExisting(MultisetElement<T> node,
			int occurrences) {
		MultisetElement<T> previous = node.getPrevious();
		int oldOccurrences = node.getOccurrences();

		if (oldOccurrences > occurrences) {
			node.decreaseOccurrences(occurrences);
			return oldOccurrences - occurrences;
		}
		MultisetElement<T> next = node.getNext();
		previous.setNext(next);
		next.setPrevious(previous);
		return oldOccurrences;
	}

	/**
	 * Removes all elements from the multiset.
	 *
	 * @param c The collection used as parameter.
	 * @return true if the operation is successful, false otherwise.
	 */
	@Override
	public boolean removeAll(Collection<?> c) {
		return collectionEraser.removeFromCollection(c);
	}
}

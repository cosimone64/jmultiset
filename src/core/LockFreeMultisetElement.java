/**
 * LockFreeMultisetElement.java
 * This class represents a single entry in a Multiset.
 *
 * Must be parameterized with a generic type T. The class
 * has default visibility, and its methods are at most
 * protected: this class is not meant to be used outside
 * of the "core" package.
 *
 * @since: (will put the date when I'm done)
 */

package core;

import java.util.concurrent.atomic.AtomicMarkableReference;

class LockFreeMultisetElement<T>
	extends AtomicMarkableReference<LockFreeMultisetElement<T>> {
	private T element;

	protected LockFreeMultisetElement(T element,
			LockFreeMultisetElement<T> next) {
		super(next, false);
		this.element = element;
	}

	protected int getElementHash() {
		return element == null ?
			Integer.MAX_VALUE :
			element.hashCode();
	}

	protected T getElement() { return element; }
}

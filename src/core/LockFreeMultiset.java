/**
 * LockFreeMultiset.java
 * This class implements a Multiset, to be parameterized
 * with a generic type T.
 *
 * Synchronization is provided through lock-free algorithms.
 * Because of issues in implementing an hashmap with this
 * synchronization, this implementation relies on a simple
 * linked list.
 *
 * @author: Cosimo Agati
 */

package core;
import java.util.Collection;

public class LockFreeMultiset<T> implements Multiset<T> {
	private LockFreeMultisetElement<T> head;
	private CollectionEraser<T> collectionEraser;

	public LockFreeMultiset() {
		head = new LockFreeMultisetElement<T>(null, null);
		collectionEraser = new CollectionEraser<T>(this);
	}

	/**
	 * Adds an element to the multiset.
	 * 
	 * @param element The element to be added.
	 * @return true if the operation is successful, false otherwise.
	 * @throws IllegalArgumentException if element == null.
	 */
	@Override
	public boolean add(T element) {
		LockFreeMultisetElement<T> next = head.getReference();
		LockFreeMultisetElement<T> newNode =
			new LockFreeMultisetElement<T>(element, next);

		if (!head.compareAndSet(next, newNode, false, false))
			return add(element);
		return true;
	}

	/**
	 * Checks if an element is currently in the multiset.
	 * 
	 * @param element The element to be checked.
	 * @return true if the operation is successful, false otherwise.
	 * @throws IllegalArgumentException if element == null.
	 */
	// Simple, clear and straightforward: this is how a method should
	// look when implementing an actual algorithm!
	@Override
	public boolean contains(Object element) {
		LockFreeMultisetElement<T> node = head.getReference();

		while(node != null) {
			if (element.equals(node.getElement()))
				return !node.isMarked();
			node = node.getReference();
		}
		return false;
	}

	/**
	 * Returns the number of occurrences of an element in the
	 * multiset.
	 * 
	 * @param element The element whose occurrences are being counted.
	 * @return The number of occurrences of element.
	 * @throws IllegalArgumentException if element == null.
	 */
	@Override
	public int count(Object element) {
		LockFreeMultisetElement<T> node = head.getReference();
		int count = 0;

		while(node != null) {
			if (element.equals(node.getElement()))
				count = !node.isMarked() ? count + 1 : count;
			node = node.getReference();
		}
		return count;
	}

	/**
	 * Removes an element from the multiset.
	 * 
	 * @param element The element to be removed
	 * @return true if the operation is successful, false otherwise.
	 * @throws IllegalArgumentException if element == null.
	 */
	@Override
	public boolean remove(Object element) {
		boolean[] mark = new boolean[1];
		LockFreeMultisetElement<T> previous = head;
		LockFreeMultisetElement<T> node = head.getReference();
		LockFreeMultisetElement<T> next;

		while (node != null) {
			next = node.get(mark);
			if (mark[0])
				if (!previous.compareAndSet(node, next,
							false, false))
					return remove(element);
			if (element.equals(node.getElement())) {
				if (!node.compareAndSet(next, next, false, true))
					return remove(element);
				previous.compareAndSet(node, next, false, false);
				return true;
			}
			previous = node;
			node = next;
		}
		return false;
	}

	/**
	 * Adds a number of occurrences of the same element to
	 * the multiset.
	 * 
	 * @param element The element to be added.
	 * @param occurrences The number of occurrences to add.
	 * @return The number of elements successfully inserted.
	 * @throws IllegalArgumentException if element == null.
	 */
	@Override
	public int add(T element, int occurrences){
		for (int i = 0; i < occurrences; ++i)
			add(element);
		return occurrences;
	}

	/**
	 * Removes a number of occurrences of the same element from
	 * the multiset.
	 * 
	 * @param element The element to be removed
	 * @param occurrences The number of occurrences to remove
	 * @return The number of elements successfully removed.
	 * @throws IllegalArgumentException if element == null.
	 */
	@Override
	public int remove(Object element, int occurrences){
		int removedElements = 0;

		for (; occurrences > 0; --occurrences)
			if (remove(element))
				++removedElements;
			else
				break;
		return removedElements;
	}

	/**
	 * Removes all elements from the multiset.
	 *
	 * @param c The collection used as parameter.
	 * @return true if the operation is successful, false otherwise.
	 */
	@Override
	public boolean removeAll(Collection<?> c) {
		return collectionEraser.removeFromCollection(c);
	}
}

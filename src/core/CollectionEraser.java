package core;

import java.util.Collection;

class CollectionEraser<T> {
	private Multiset<T> multiset;

	protected CollectionEraser(Multiset<T> multiset) {
		this.multiset = multiset;
	}

	protected boolean removeFromCollection(Collection<?> collection) {
		int i = 0;

		for (Object element : collection)
			multiset.remove(element);
		return true;
	}
}

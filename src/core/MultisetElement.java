/**
 * MultisetElement.java
 * This class represents a single entry in a Multiset.
 *
 * Must be parameterized with a generic type T. The class
 * has default visibility, and its methods are at most
 * protected: this class is not meant to be used outside
 * of the "core" package.
 *
 * @author: Cosimo Agati
 */

package core;

import java.util.concurrent.locks.ReentrantLock;

class MultisetElement<T> {
	private MultisetElement<T> next;
	private MultisetElement<T> previous;
	private T element;
	private ReentrantLock elementLock;
	private int occurrences;
	private boolean mark;

	protected MultisetElement(T element,
				  int occurrences,
				  MultisetElement<T> previous,
				  MultisetElement<T> next) {
		this.element = element;
		this.occurrences = occurrences;
		this.previous = previous;
		this.next = next;
		mark = false;
		elementLock = new ReentrantLock();
	}

	protected MultisetElement(T element, int occurrences) {
		this(element, occurrences, null, null);
	}

	protected MultisetElement(T element) { this(element, 1); }

	protected MultisetElement<T> getNext() { return next; }

	protected MultisetElement<T> getPrevious() {
		return previous;
	}

	protected MultisetElement<T> setNext(MultisetElement<T> e) {
		return next = e;
	}

	protected MultisetElement<T> setPrevious(
		MultisetElement<T> e) {
		return previous = e;
	}

	protected int getElementHash() {
		return element == null ?
			Integer.MAX_VALUE :
			element.hashCode();
	}

	protected int setOccurrences(int o) {
		return occurrences = o;
	}

	protected boolean mark() { return mark = true; }

	protected boolean unmark() { return mark = false; }

	protected boolean isMarked() { return mark; }

	protected synchronized boolean lock() {
		elementLock.lock();
		return true;
	}

	protected boolean unlock() {
		elementLock.unlock();
		return true;
	}

	protected T getElement() { return element; }

	protected int getOccurrences() { return occurrences; }

	protected int increaseOccurrences(int increment) {
		return occurrences += increment;
	}

	protected int decreaseOccurrences(int decrement) {
		return occurrences -= decrement;
	}
}

/**
 * LazyMultiset.java
 * This class implements a Multiset, to be parameterized
 * with a generic type T.
 *
 * Synchronization is provided through lazy locking techniques.
 *
 * @author: Cosimo Agati
 */

package core;
import java.util.Collection;

public class LazyMultiset<T> implements Multiset<T> {
	private MultisetElement<T>[] buckets;
	private CollectionEraser<T> collectionEraser;

	@SuppressWarnings({"unchecked", "rawtypes"})
	public LazyMultiset(int size) {
		buckets = new MultisetElement[size];
		collectionEraser = new CollectionEraser<T>(this);

		for (int i = 0; i < size; ++i) {
			buckets[i] = new MultisetElement<T>(null);
			MultisetElement<T> next = new MultisetElement<T>(null);
			buckets[i].setNext(next);
			next.setPrevious(buckets[i]);
		}
	}

	/*
	 * Constructor with default size.
	 */
	public LazyMultiset() { this(1024); }

	/*
	 * Gets the bucket index to start looking for the element.
	 */
	private int getBucketIndex(Object element) {
		return element.hashCode() % buckets.length;
	}

	/*
	 * Returns a node whose element has an hashcode greater
	 * or equal than the element to look for.
	 */
	private MultisetElement<T> lookup(Object element) {
		final int BUCKET_INDEX = getBucketIndex(element);
		final int ELEMENT_HASH = element.hashCode();
		MultisetElement<T> previous = buckets[BUCKET_INDEX];
		MultisetElement<T> node = previous.getNext();

		while (node.getElementHash() < ELEMENT_HASH) {
			previous = node;
			node = node.getNext();
		}
		return node;
	}

	/**
	 * Adds an element to the multiset.
	 * 
	 * @param element The element to be added.
	 * @return true if the operation is successful, false otherwise.
	 * @throws IllegalArgumentException if element == null.
	 */
	@Override
	public boolean add(T element) {
		return add(element, 1) > 0;
	}

	/**
	 * Checks if an element is currently in the multiset.
	 * 
	 * @param element The element to be checked.
	 * @return true if the operation is successful, false otherwise.
	 * @throws IllegalArgumentException if element == null.
	 */
	@Override
	public boolean contains(Object element) {
		final int ELEMENT_HASH = element.hashCode();
		MultisetElement<T> node = lookup(element);

		return node.getElementHash() == ELEMENT_HASH
			&& !node.isMarked();
	}


	/**
	 * Returns the number of occurrences of an element in the
	 * multiset.
	 * 
	 * @param element The element whose occurrences are being counted.
	 * @return The number of occurrences of element.
	 * @throws IllegalArgumentException if element == null.
	 */
	@Override
	public int count(Object element) {
		final int ELEMENT_HASH = element.hashCode();
		MultisetElement<T> node = lookup(element);

		if (node.getElementHash() == ELEMENT_HASH)
			return node.getOccurrences();
		return 0;
	}

	/**
	 * Removes an element from the multiset.
	 * 
	 * @param element The element to be removed
	 * @return true if the operation is successful, false otherwise.
	 * @throws IllegalArgumentException if element == null.
	 */
	@Override
	public boolean remove(Object element) {
		return remove(element, 1) > 0;
	}

	/**
	 * Adds a number of occurrences of the same element to
	 * the multiset.
	 * 
	 * @param element The element to be added.
	 * @param occurrences The number of occurrences to add.
	 * @return The number of elements successfully inserted.
	 * @throws IllegalArgumentException if element == null.
	 */
	@Override
	public int add(T element, int occurrences) {
		MultisetElement<T> node = lookup(element);
		MultisetElement<T> previous = node.getPrevious();
		final int ELEMENT_HASH = element.hashCode();

		previous.lock();
		node.lock();
		if (previous.isMarked() || previous.getNext() != node) {
			previous.unlock();
			node.unlock();
			return add(element, occurrences);
		}
		if (node.getElementHash() == ELEMENT_HASH) {
			node.increaseOccurrences(occurrences);
			previous.unlock();
			node.unlock();
			return occurrences;
		}
		MultisetElement<T> newNode = new MultisetElement<T>(element,
				occurrences, previous, node);
		previous.setNext(newNode);
		node.setPrevious(newNode);
		previous.unlock();
		node.unlock();
		return occurrences;
	}

	/**
	 * Removes a number of occurrences of the same element from
	 * the multiset.
	 * 
	 * @param element The element to be removed
	 * @param occurrences The number of occurrences to remove
	 * @return The number of elements successfully removed.
	 * @throws IllegalArgumentException if element == null.
	 */
	@Override
	public int remove(Object element, int occurrences) {
		MultisetElement<T> node = lookup(element);
		MultisetElement<T> previous = node.getPrevious();
		final int ELEMENT_HASH = element.hashCode();

		previous.lock();
		node.lock();
		if (previous.isMarked() || previous.getNext() != node) {
			previous.unlock();
			node.unlock();
			return remove(element, occurrences);
		}
		if (node.getElementHash() > ELEMENT_HASH) {
			previous.unlock();
			node.unlock();
			return 0;
		}
		return removeElements(node, occurrences);
	}

	/*
	 * If there are enough elements, decrease the occurrences in the node
	 * and release the locks. Otherwise, mark the node and redirect
	 * pointers.
	 */
	private int removeElements(MultisetElement<T> node, int numToRemove) {
		int oldOccurrences = node.getOccurrences();

		if (numToRemove < oldOccurrences) {
			node.decreaseOccurrences(numToRemove);
			MultisetElement<T> previous = node.getPrevious();
			previous.unlock();
			node.unlock();
			return numToRemove;
		}
		deallocateAndMark(node);
		return oldOccurrences;
	}

	/*
	 * Marks node, redirects its predecessor to its successor
	 * and releases the locks.
	 */
	private boolean  deallocateAndMark(MultisetElement<T> node) {
		MultisetElement<T> next = node.getNext();
		MultisetElement<T> previous = node.getPrevious();

		node.mark();
		previous.setNext(next);
		next.setPrevious(previous);
		previous.unlock();
		node.unlock();
		return true;
	}

	/**
	 * Removes all elements from the multiset.
	 *
	 * @param c The collection used as parameter.
	 * @return true if the operation is successful, false otherwise.
	 */
	@Override
	public boolean removeAll(Collection<?> c) {
		return collectionEraser.removeFromCollection(c);
	}
}

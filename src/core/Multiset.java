/**
 * Multiset.java
 * This interface represents a Multiset, to be parameterized
 * with a generic type T.
 *
 * @author: Cosimo Agati
 */

package core;

import java.util.Collection;

public interface Multiset<T> {
	/**
	 * Adds an element to the multiset.
	 * 
	 * @param element The element to be added.
	 * @return true if the operation is successful, false otherwise.
	 * @throws IllegalArgumentException if element == null.
	 */
	public boolean add(T element);

	/**
	 * Checks if an element is currently in the multiset.
	 * 
	 * @param element The element to be checked.
	 * @return true if the operation is successful, false otherwise.
	 * @throws IllegalArgumentException if element == null.
	 */
	public boolean contains(Object element);

	/**
	 * Returns the number of occurrences of an element in the
	 * multiset.
	 * 
	 * @param element The element whose occurrences are being counted.
	 * @return The number of occurrences of element.
	 * @throws IllegalArgumentException if element == null.
	 */
	public int count(Object element);

	/**
	 * Removes an element from the multiset.
	 * 
	 * @param element The element to be removed
	 * @return true if the operation is successful, false otherwise.
	 * @throws IllegalArgumentException if element == null.
	 */
	public boolean remove(Object element);

	/**
	 * Adds a number of occurrences of the same element to
	 * the multiset.
	 * 
	 * @param element The element to be added.
	 * @param occurrences The number of occurrences to add.
	 * @return The number of elements successfully inserted.
	 * @throws IllegalArgumentException if element == null.
	 */
	public int add(T element, int occurrences);

	/**
	 * Removes a number of occurrences of the same element from
	 * the multiset.
	 * 
	 * @param element The element to be removed
	 * @param occurrences The number of occurrences to remove
	 * @return The number of elements successfully removed.
	 * @throws IllegalArgumentException if element == null.
	 */
	public int remove(Object element, int occurrences);

	/**
	 * Removes all elements from the multiset.
	 *
	 * @param c The collection used as parameter.
	 * @return true if the operation is successful, false otherwise.
	 */
	public boolean removeAll(Collection<?> c);
}


package test;

import core.*;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

class ContainsTestThread extends Thread {
	private int beginIndex;
	private boolean functionalityTest;
	private Multiset<Integer> testMultiset;
	private CyclicBarrier barrier;
	
	public ContainsTestThread(
			int beginIndex,
			boolean functionalityTest,
			Multiset<Integer> testMultiset,
			CyclicBarrier barrier) {
		this.beginIndex = beginIndex;
		this.functionalityTest = functionalityTest;
		this.testMultiset = testMultiset;
		this.barrier = barrier;
	}
	
	public void run() {
		for (int i = beginIndex; i < Test.getSize(); i += Test.getThreadNum()) {
			if (functionalityTest)
				try {
					barrier.await();
				} catch (InterruptedException | BrokenBarrierException e) {
					e.printStackTrace(System.out);
				}
			try {
				testMultiset.contains(i);
			} catch (NullPointerException e1) {
				e1.printStackTrace(System.out);
			}
		}
	}
}

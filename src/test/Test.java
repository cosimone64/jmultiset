package test;

import core.*;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CyclicBarrier;

public class Test {
	private static final int THREAD_NUM = 500;
	private static final int CAPACITY = 10000;
	private static final int SIZE = 10000;
	
	public static void main(String[] args) {
		Multiset<Integer> testMultiset = null;
		CyclicBarrier barrier = new CyclicBarrier(THREAD_NUM);
		List<Thread> threads;
		long completionTime;
		
		for (int i = 0; i < 3; i++) {
			switch (i) {
			case 0:	System.out.println("FINE GRAINED VERSION TEST\n");
					testMultiset = new FineGrainedMultiset<Integer>(CAPACITY);
					break;
			case 1: System.out.println("\nLAZY VERSION TEST\n");
					testMultiset = new LazyMultiset<Integer>(CAPACITY);
					break;
			case 2: System.out.println("\nLOCK FREE VERSION TEST\n");
					testMultiset = new LockFreeMultiset<Integer>();
					break;
			}
			// FUNCTIONALITY ADD
			
			System.out.println("Functionality Add (successful if no exception is raised)");
			threads = new LinkedList<Thread>();
			
			for (int j = 0; j < THREAD_NUM; j++)
				threads.add(new AddTestThread(j, true, testMultiset, barrier));
			for (Thread t : threads)
				t.start();
			for (Thread t : threads) {
				try {
					t.join();
				}
				catch (InterruptedException
						| NullPointerException e) {
					e.printStackTrace(System.out);
				}
			}

			// FUNCTIONALITY REMOVE
			
			System.out.println("Functionality Remove (successful if no exception is raised)");
			
			threads = new LinkedList<Thread>();
			
			for (int j = 0; j < THREAD_NUM; j++)
				threads.add(new RemoveTestThread(j, true, testMultiset, barrier));
			for (Thread t : threads)
				t.start();
			for (Thread t : threads) {
				try {
					t.join();
				} catch (InterruptedException
						| NullPointerException e) {
					e.printStackTrace(System.out);
				}
			}

			// FUNCTIONALITY CONTAINS
			
			System.out.println("Functionality Contains (successful if no exception is raised)");
			
			threads = new LinkedList<Thread>();
			
			for (int j = 0; j < THREAD_NUM; j++)
				threads.add(new ContainsTestThread(j, true, testMultiset, barrier));
			for (Thread t : threads)
				t.start();
			for (Thread t : threads) {
				try {
					t.join();
				} catch (InterruptedException
						| NullPointerException e) {
					e.printStackTrace(System.out);
				}
			}

			// FUNCTIONALITY COUNT
			
			System.out.println("Functionality Count (successful if no exception is raised)");
			
			threads = new LinkedList<Thread>();
			
			for (int j = 0; j < THREAD_NUM; j++)
				threads.add(new CountTestThread(j, true, testMultiset, barrier));
			for (Thread t : threads)
				t.start();
			for (Thread t : threads) {
				try {
					t.join();
				} catch (InterruptedException
						| NullPointerException e) {
					e.printStackTrace(System.out);
				}
			}

			// FUNCTIONALITY REMOVEALL
			
			System.out.println("Functionality RemoveAll (successful if no exception is raised)");
			threads = new LinkedList<Thread>();
			
			for (int j = 0; j < THREAD_NUM; j++)
				threads.add(new RemoveAllTestThread(j, true, testMultiset, barrier));
			for (Thread t : threads)
				t.start();
			for (Thread t : threads) {
				try {
					t.join();
				}
				catch (InterruptedException
						| NullPointerException e) {
					e.printStackTrace(System.out);
				}
			}

			// ADD
			
			threads = new LinkedList<Thread>();
			completionTime = System.currentTimeMillis();
			
			for (int j = 0; j < THREAD_NUM; j++)
				threads.add(new AddTestThread(j, false, testMultiset, barrier));
			for (Thread t : threads)
				t.start();
			for (Thread t : threads) {
				try {
					t.join();
				}
				catch (InterruptedException
						| NullPointerException e) {
					e.printStackTrace(System.out);
				}
			}
			completionTime = System.currentTimeMillis() - completionTime;
			System.out.println("Add completion time: " + completionTime);
			
			// REMOVE
			
			threads = new LinkedList<Thread>();
			completionTime = System.currentTimeMillis();
			
			for (int j = 0; j < THREAD_NUM; j++)
				threads.add(new AddTestThread(j, false, testMultiset, barrier));
			for (Thread t : threads)
				t.start();
			for (Thread t : threads) {
				try {
					t.join();
				}
				catch (InterruptedException
						| NullPointerException e) {
					e.printStackTrace(System.out);
				}
			}
			completionTime = System.currentTimeMillis() - completionTime;
			System.out.println("Remove completion time: " + completionTime);

			// CONTAINS

			threads = new LinkedList<Thread>();
			completionTime = System.currentTimeMillis();
			
			for (int j = 0; j < THREAD_NUM; j++)
				threads.add(new ContainsTestThread(j, false, testMultiset, barrier));
			for (Thread t : threads)
				t.start();
			for (Thread t : threads) {
				try {
					t.join();
				}
				catch (InterruptedException
						| NullPointerException e) {
					e.printStackTrace(System.out);
				}
			}
			completionTime = System.currentTimeMillis() - completionTime;
			System.out.println("Contains completion time: " + completionTime);

			// COUNT

			threads = new LinkedList<Thread>();
			completionTime = System.currentTimeMillis();
			
			for (int j = 0; j < THREAD_NUM; j++)
				threads.add(new CountTestThread(j, false, testMultiset, barrier));
			for (Thread t : threads)
				t.start();
			for (Thread t : threads) {
				try {
					t.join();
				}
				catch (InterruptedException
						| NullPointerException e) {
					e.printStackTrace(System.out);
				}
			}
			completionTime = System.currentTimeMillis() - completionTime;
			System.out.println("Count completion time: " + completionTime);

			// REMOVEALL
			
			threads = new LinkedList<Thread>();
			completionTime = System.currentTimeMillis();
			
			for (int j = 0; j < THREAD_NUM; j++)
				threads.add(new RemoveAllTestThread(j, false, testMultiset, barrier));
			for (Thread t : threads)
				t.start();
			for (Thread t : threads) {
				try {
					t.join();
				}
				catch (InterruptedException
						| NullPointerException e) {
					e.printStackTrace(System.out);
				}
			}
			completionTime = System.currentTimeMillis() - completionTime;
			System.out.println("RemoveAll completion time: " + completionTime);
		}
	}

	public static int getThreadNum() { return THREAD_NUM; }

	public static int getCapacityI() { return CAPACITY; }

	public static int getSize() { return SIZE; }
}

package test;

import core.*;
import java.util.Collection;
import java.util.ArrayList;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

class RemoveAllTestThread extends Thread {
	private int beginIndex;
	private boolean functionalityTest;
	private Multiset<Integer> testMultiset;
	private CyclicBarrier barrier;
	private Collection<Integer> c;
	
	public RemoveAllTestThread(
			int beginIndex,
			boolean functionalityTest,
			Multiset<Integer> testMultiset,
			CyclicBarrier barrier) {
		this.beginIndex = beginIndex;
		this.functionalityTest = functionalityTest;
		this.testMultiset = testMultiset;
		this.barrier = barrier;
		c = new ArrayList<Integer>();
	}
	
	public void run() {
		for (int i = 0; i < 10000; ++i)
			c.add(i);
		for (int i = beginIndex; i < Test.getSize();
				i += Test.getThreadNum()) {
			if (functionalityTest) {
				try {
					barrier.await();
				}
				catch (InterruptedException
						| BrokenBarrierException e) {
					e.printStackTrace(System.out);
				}
			}
			testMultiset.removeAll(c);
		}
	}
}

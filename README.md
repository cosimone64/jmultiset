# jMultiset

## Concurrent multiset implementations

A multiset (also called a bag) is an extension of the mathematical set
concept: it is a data structure that allows multuple copies of the
same element to be stored. Unlike a list, however, it does not enforce an
order. Here, elements are considered equal if they are deemed so by the
equals() method.

## Synchronization

Three synchronization strategies are provided:
+ fine grained locking;
+ lazy synchronization;
+ lock-free synchronization.

## Internal representation

The fine grained and lazy versions are internally represented as a hashtable,
while the lock-free one, due to the more complex synchronization algorithm,
is still a simple linked list. It will (hopefully!) be converted to a
hashtable in the future.

## Tests

A sample test class is provided, to measure the performance of all
threee versions.
